﻿using Sasw.PaginationTester.Extensions;
using Sasw.PaginationTester.Services;

namespace Sasw.PaginationTester;

public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services
            .AddTransient<TicketService>()
            .AddCors(Constants.Cors.PolicyName)
            .AddOpenApi()
            .AddControllers();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        
        app.UseOpenApi();
        app.UseRouting();
        app.UseCors(Constants.Cors.PolicyName);
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}