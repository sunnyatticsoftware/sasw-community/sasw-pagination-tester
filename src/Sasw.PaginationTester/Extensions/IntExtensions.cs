﻿namespace Sasw.PaginationTester.Extensions;

public static class IntExtensions
{
    public static Guid ToGuid(this int value)
    {
        var bytes = new byte[16];
        BitConverter.GetBytes(value).CopyTo(bytes, 0);
        var result = new Guid(bytes);
        return result;
    }
}