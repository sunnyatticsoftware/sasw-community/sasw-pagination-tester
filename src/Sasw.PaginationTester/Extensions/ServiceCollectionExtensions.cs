﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Sasw.PaginationTester.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddCors(this IServiceCollection services, string policyName)
    {
        services
            .AddCors(
                options =>
                {
                    options
                        .AddPolicy(
                            policyName,
                            builder =>
                            {
                                builder
                                    .WithOrigins("*")
                                    .AllowAnyHeader()
                                    .AllowAnyMethod();
                            });
                });

        return services;
    }

    public static IServiceCollection AddOpenApi(this IServiceCollection services)
    {
        var mainAssemblyName = typeof(Startup).Assembly.GetName().Name;

        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = mainAssemblyName,
                Version = "v1",
                Description = "Sasw Pagination Tester",
                Contact = new OpenApiContact
                {
                    Name = "Diego"
                }
            });

            c.DocumentFilter<LowerCaseDocumentFilter>();

            var xmlCommentsWebApi = Path.Combine(AppContext.BaseDirectory, $"{mainAssemblyName}.xml");
            c.IncludeXmlComments(xmlCommentsWebApi);
        });

        return services;
    }

    // ReSharper disable once ClassNeverInstantiated.Local
    private sealed class LowerCaseDocumentFilter
        : IDocumentFilter
    {
        private static string LowercaseEverythingButParameters(string key) => string.Join('/', key.Split('/').Select(x => x.Contains("{") ? x : x.ToLower()));
    
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            var paths = swaggerDoc.Paths.ToDictionary(entry => LowercaseEverythingButParameters(entry.Key),
                entry => entry.Value);
            swaggerDoc.Paths = new OpenApiPaths();
            foreach (var (key, value) in paths)
            {
                swaggerDoc.Paths.Add(key, value);
            }
        }
    }
}

// ReSharper disable once ClassNeverInstantiated.Global