using Microsoft.AspNetCore.Mvc;
using Sasw.PaginationTester.Models;
using Sasw.PaginationTester.Services;

namespace Sasw.PaginationTester.Controllers;

[Route("api/[controller]")]
public class TicketsController
    : ControllerBase
{
    private readonly TicketService _ticketService;

    public TicketsController(TicketService ticketService)
    {
        _ticketService = ticketService;
    }
    
    [HttpGet]
    public async Task<IActionResult> GetTickets([FromQuery]Pagination pagination)
    {
        var pagedResult = await _ticketService.GetTickets(pagination);
        return Ok(pagedResult);
    }
}