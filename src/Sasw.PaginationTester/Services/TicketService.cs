using Sasw.PaginationTester.Extensions;
using Sasw.PaginationTester.Models;

namespace Sasw.PaginationTester.Services;

public class TicketService
{
    public Task<PagedResult<Ticket>> GetTickets(Pagination pagination)
    {
        var pageNumber = pagination.Page;
        var pageSize = pagination.Count;

        var allTickets = GetSampleData().ToList();
        
        var count = allTickets.Count();
        if (count == 0)
        {
            var defaultResult =
                new PagedResult<Ticket>
                {
                    Items = Enumerable.Empty<Ticket>(),
                    PageNumber = pageNumber,
                    PageSize = pageSize,
                    TotalPages = pageNumber,
                    TotalCount = count
                };
            
            return Task.FromResult(defaultResult);
        }
        
        var skipCount = pageSize * (pageNumber - 1);
        var totalPages = (int)Math.Ceiling((double)count/ pageSize);

        var items = allTickets.Skip(skipCount).Take(pageSize);

        var result =
            new PagedResult<Ticket>
            {
                Items = items,
                PageNumber = pageNumber,
                PageSize = pageSize,
                TotalPages = totalPages,
                TotalCount = count
            };

        return Task.FromResult(result);
    }

    private static IEnumerable<Ticket> GetSampleData()
    {
        var tickets =
            new List<Ticket>
            {
                new Ticket(1.ToGuid(), Channel.Chat, "one"),
                new Ticket(2.ToGuid(), Channel.Email, "two"),
                new Ticket(3.ToGuid(), Channel.Email, "three"),
                new Ticket(4.ToGuid(), Channel.Phone, "four"),
                new Ticket(5.ToGuid(), Channel.Chat, "five"),
                new Ticket(6.ToGuid(), Channel.Chat, "six"),
                new Ticket(7.ToGuid(), Channel.Email, "seven"),
                new Ticket(8.ToGuid(), Channel.Chat, "eight"),
                new Ticket(9.ToGuid(), Channel.Email, "nine"),
                new Ticket(10.ToGuid(), Channel.Email, "ten"),
                new Ticket(11.ToGuid(), Channel.Chat, "eleven"),
                new Ticket(12.ToGuid(), Channel.Chat, "twelve"),
                new Ticket(13.ToGuid(), Channel.Chat, "thirteen"),
                new Ticket(14.ToGuid(), Channel.Email, "fourteen"),
                new Ticket(15.ToGuid(), Channel.Chat, "fifteen"),
                new Ticket(16.ToGuid(), Channel.Email, "sixteen"),
                new Ticket(17.ToGuid(), Channel.Chat, "seventeen"),
                new Ticket(18.ToGuid(), Channel.Email, "eighteen"),
                new Ticket(19.ToGuid(), Channel.Phone, "nineteen"),
                new Ticket(20.ToGuid(), Channel.Chat, "twenty"),
                new Ticket(21.ToGuid(), Channel.Email, "twenty-one"),
                new Ticket(22.ToGuid(), Channel.Chat, "twenty-two"),
                new Ticket(23.ToGuid(), Channel.Chat, "twenty-three"),
                new Ticket(24.ToGuid(), Channel.Chat, "twenty-four"),
                new Ticket(25.ToGuid(), Channel.Phone, "twenty-five"),
                new Ticket(26.ToGuid(), Channel.Chat, "twenty-six"),
                new Ticket(27.ToGuid(), Channel.Chat, "twenty-seven"),
                new Ticket(28.ToGuid(), Channel.Chat, "twenty-eight"),
                new Ticket(29.ToGuid(), Channel.Email, "twenty-nine"),
                new Ticket(30.ToGuid(), Channel.Phone, "thirty"),
                new Ticket(31.ToGuid(), Channel.Email, "thirty-one"),
                new Ticket(32.ToGuid(), Channel.Chat, "thirty-two")
            };
        return tickets;
    }
}