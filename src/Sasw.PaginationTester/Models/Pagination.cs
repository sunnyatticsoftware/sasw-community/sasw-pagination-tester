namespace Sasw.PaginationTester.Models;

public class Pagination
{
    public int Page { get; init; } = 1;
    public int Count { get; init; } = 20;
}