namespace Sasw.PaginationTester.Models;

public class PagedResult<TDocument>
    where TDocument : class
{
    public IEnumerable<TDocument> Items { get; init; } = Enumerable.Empty<TDocument>();
    public int PageNumber { get; init; }
    public int PageSize { get; init; }
    public int TotalPages { get; init; }
    public long TotalCount { get; init; }

    public override string ToString()
    {
        return $"{nameof(PageNumber)}={PageNumber}, " +
               $"{nameof(PageSize)}={PageSize}, " +
               $"{nameof(TotalPages)}={TotalPages}, " +
               $"{nameof(TotalCount)}={TotalCount}, " +
               $"{nameof(Items)}={Items}";
    }
}