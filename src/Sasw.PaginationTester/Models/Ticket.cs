namespace Sasw.PaginationTester.Models;

public record Ticket(Guid Id, String Channel, String Subject);