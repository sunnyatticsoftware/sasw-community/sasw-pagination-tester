namespace Sasw.PaginationTester.Models;

public static class Channel
{
    public static string Chat = "chat";
    public static string Email = "email";
    public static string Phone = "phone";
}