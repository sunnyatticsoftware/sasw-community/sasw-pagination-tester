(() => {

  const gridOptions = {
    columnDefs: [
      { valueGetter: 'node.rowIndex', maxWidth: 100 },
      { field: 'id', minWidth: 150 },
      { field: 'channel', minWidth: 150 },
      { field: 'subject', minWidth: 400 }
    ],
  
    defaultColDef: {
      flex: 1,
      minWidth: 80,
    },
  
    rowModelType: 'serverSide',
    serverSideInfiniteScroll: true,
    cacheBlockSize: 10, //this is the count (page size)
    maxBlocksInCache: 2,
    rowBuffer: 0,
    debug: false
  };
  

  document.addEventListener('DOMContentLoaded', function () {
    var gridDiv = document.querySelector('#myGrid');
    new agGrid.Grid(gridDiv, gridOptions);
    var datasource = createDataSource();
    gridOptions.api.setServerSideDatasource(datasource);
  });

  function createDataSource() {
    return {
      getRows: (params) => {
        let count = params.request.endRow - params.request.startRow; // or the cacheBlockSize
        let page = Math.ceil(params.request.endRow / count);

        console.log(`asking for page ${page} and count ${count}`);

        fetch(`http://localhost:5000/api/tickets?page=${page}&count=${count}`)
          .then((response) => response.json())
          .then(data => {

            params.success({
              rowData: data.items,
              rowCount: data.totalCount,
            });
          });
      },
    };
  }

})();


  