# sasw-pagination-tester

Simple web api to test pagination.

## How to use as a docker container
IMPORTANT: The docker image exposes port `8080` and it adds the `ENV ASPNETCORE_URLS=http://*:8080` so that this is the port AspNetCore is listening on.

This app is stored as a docker image in a public repository, so no need for GitLab credentials.

Run the container by mapping it to the desired localhost port (e.g: `5000` or any other)

```
docker run -p 5000:8080 registry.gitlab.com/sunnyatticsoftware/sasw-community/sasw-pagination-tester:latest
```

Open the swagger to interact [http://localhost:5000/swagger](http://localhost:5000/swagger)